﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameOver : MonoBehaviour,IPointerClickHandler {

	public Text result;

	void OnEnable () {
		if(GameController.main.winner != -1)
			result.text = "Player "+(GameController.main.winner+1)+" win";
		else result.text = "Draw";
	}

	public void OnPointerClick(PointerEventData eventData){
		ScoreController.main.UpdateScore ();
		GameController.main.StartGame ();
		gameObject.SetActive (false);
	}
}
