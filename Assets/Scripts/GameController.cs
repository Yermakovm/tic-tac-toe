﻿using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public static GameController main;

	public static int complexity;

	public GameObject gameOver;

	public GameObject firstPlayerIcon;
	public GameObject secondPlayerIcon;

	public int winner;

	public int turn;
	public List<Cell> cells;


	private int m_activePlayer;
	private List<int> m_checkForWin;

	void Start () {
		main = this;
		m_checkForWin = new List<int>();
		StartGame ();
	}

	public void StartGame(){

		turn = 0;

		foreach (Cell c in cells) {
			c.resetCell ();
		}

		if(winner != -1)
			m_activePlayer = winner;

		SetIcon (m_activePlayer);

	}

	public void NextTun(){

		turn++;

		if (CheckForWin ()) {
			gameOver.SetActive(true);
			return;
		}
		m_activePlayer = (m_activePlayer + 1) % 2;
		SetIcon (m_activePlayer);

		if (!AvailableMoves ()) {
			gameOver.SetActive(true);
			return;
		}
	}

	public void SetIcon(int player){
		if (player == 0) {
			firstPlayerIcon.SetActive (true);
			secondPlayerIcon.SetActive (false);
		} else {
			firstPlayerIcon.SetActive (false);
			secondPlayerIcon.SetActive (true);
		}
	}

	public bool AvailableMoves(){
		foreach (Cell c in cells) {
			if (c.potential == 0)
				return true;
		}
		winner = -1;
		return false;
	}

	public bool CheckForWin(){

		for(int i = 0; i<3;i++){
			m_checkForWin.Add(cells [3*i].potential + cells [3*i+1].potential + cells [3*i+2].potential);
			m_checkForWin.Add(cells [i].potential + cells [i+3].potential + cells [i+6].potential);
		}

		m_checkForWin.Add(cells [0].potential + cells [4].potential + cells [8].potential);
		m_checkForWin.Add(cells [2].potential + cells [4].potential + cells [6].potential);

		foreach (var v in m_checkForWin) {
			if (Mathf.Abs (v) == 3) {
				winner = m_activePlayer;
				m_checkForWin.Clear ();
				return true;
			}
		}

		m_checkForWin.Clear();

		return false;
	}
}
