﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	public static ScoreController main;

	public Text player1;
	public Text player2;
	public Text draw;

	private int m_p1Score;
	private int m_p2Score;
	private int m_draw;

	void Start(){
		main = this;
	}

	public void UpdateScore () {
		if (GameController.main.winner == 0) {
			m_p1Score += 1;
			player1.text = m_p1Score.ToString ();
		} else if (GameController.main.winner == 1) {
			m_p2Score += 1;
			player2.text = m_p2Score.ToString ();
		} else {
			m_draw += 1;
			draw.text = m_draw.ToString ();
		}
	}
}
