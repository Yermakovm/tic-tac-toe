﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Cell : MonoBehaviour, IPointerClickHandler {

	public Sprite x,o;
	public int potential;

	public void OnPointerClick(PointerEventData eventData){
		if(gameObject.GetComponent<Image> ().sprite == null)
			SetSprite ();
	}

	public void SetSprite(){
		if (GameController.main.turn%2 == 0) {
			potential = 1;
			gameObject.GetComponent<Image> ().sprite = x;
		} else {
			potential = -1;
			gameObject.GetComponent<Image> ().sprite = o;
		}
		GameController.main.NextTun();
	}

	public void resetCell(){
		potential = 0;
		gameObject.GetComponent<Image> ().sprite = null;
	}
}