﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public void LoadGame(int i){
		GameController.complexity = i;
		SceneManager.LoadScene ("Game");
	}
}
